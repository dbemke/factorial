#include <iostream>
using namespace std;


double silnia(int n) {
	double wynik;  // zeby wynik dzialal na zewnatrz
	if (n == 0) {
		wynik = 1;
	}
	else {
		wynik = 1;
		for (int i = 1; i <= n; i++) {
			wynik = wynik * i;
		}
		
	}
	return wynik;
}

double silnia_rekurencyjnie(int n) {
	if (n == 0) {
		return 1;
	}
	else {
		return n * silnia_rekurencyjnie(n - 1);
	}
}

int main() {
	int n;
	cout << " Podaj liczbe, ktorej silnie nalezy policzyc" << endl;
	cin >> n;

	if (n >= 0) {
		cout << "Obliczam " << n << "!" << endl;
		double wynik;
		wynik = silnia(n);
		cout << "wynik wynosi " << wynik << endl;

		// silnia rekurencyjnie
		wynik = silnia_rekurencyjnie(n);
		cout << "wynik silni rekurencyjnie" << wynik << endl;
	}
	else {
		cout << "Blad! Liczba musi byc wieksza lub rowna zero!";
	}

}